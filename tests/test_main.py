import jwt
import pytest

from app.app import GOOGLE_ACCOUNT, SECRET_KEY, db
from app.main import (
    get_temporary_token,
    notification_database_api,
    send_mail,
    task_database_api,
    user_database_api,
)
from app.models import Notification, Task, User
from tests.conftest import (
    create_random_notification,
    create_random_string,
    create_random_task,
    create_random_user,
)


class TestUserApi:
    def test_should_return_correct_user(self, app, truncate_database):
        with app.app_context():
            random_user = create_random_user()
            db.session.add(random_user)
            db.session.commit()

            actual_value = user_database_api.get_by_id(random_user.id)
            expected_value = random_user
            assert actual_value == expected_value

    def test_should_add_correct_user(self, app, truncate_database):
        with app.app_context():
            random_user = create_random_user()
            user_database_api.add(random_user)
            actual_value = User.query.get(random_user.id)
            expected_value = random_user
            assert actual_value == expected_value

    def test_should_update_correct_user(
        self, app, seed_tables_with_random_data, truncate_database
    ):
        with app.app_context():
            old_user = User.query.order_by(db.func.random()).first()
            new_user = create_random_user()
            user_database_api.update_whole_by_id(id=old_user.id, item_to_put=new_user)
            db.session.commit()

            updated_user = User.query.get(old_user.id)

            assert updated_user.name == new_user.name
            assert updated_user.email == new_user.email

    def test_should_delete_correct_user(
        self, app, seed_tables_with_random_data, truncate_database
    ):
        with app.app_context():
            random_user = User.query.order_by(db.func.random()).first()
            user_database_api.delete_by_id(random_user.id)
            actual_value = User.query.count()
            expected_value = 0
            assert actual_value == expected_value


class TestTaskApi:
    def test_should_return_correct_task(
        self, app, seed_tables_with_random_data, truncate_database
    ):
        with app.app_context():
            random_task = Task.query.order_by(db.func.random()).first()

            actual_value = task_database_api.get_by_id(random_task.id)
            expected_value = random_task
            assert actual_value == expected_value

    def test_should_add_correct_task(
        self, app, seed_tables_with_random_data, truncate_database
    ):
        with app.app_context():
            random_user = User.query.order_by(db.func.random()).first()
            random_task = create_random_task(user_id=random_user.id)
            task_database_api.add(random_task)
            actual_value = Task.query.get(random_task.id)
            expected_value = random_task
            assert actual_value == expected_value

    def test_should_update_correct_task(
        self, app, seed_tables_with_random_data, truncate_database
    ):
        with app.app_context():
            old_task = Task.query.order_by(db.func.random()).first()
            new_task = create_random_task(user_id=old_task.user_id)
            task_database_api.update_whole_by_id(id=old_task.id, item_to_put=new_task)
            db.session.commit()

            updated_task = Task.query.get(old_task.id)

            assert updated_task.name == new_task.name
            assert updated_task.note == new_task.note
            assert updated_task.date_and_time == new_task.date_and_time
            assert updated_task.user_id == new_task.user_id

    def test_should_delete_correct_task(
        self, app, seed_tables_with_random_data, truncate_database
    ):
        with app.app_context():
            random_task = Task.query.order_by(db.func.random()).first()
            task_database_api.delete_by_id(random_task.id)
            actual_value = Task.query.count()
            expected_value = 0
            assert actual_value == expected_value


class TestNotificationApi:
    def test_should_return_correct_notification(
        self, app, seed_tables_with_random_data, truncate_database
    ):
        with app.app_context():
            random_notification = Notification.query.order_by(db.func.random()).first()

            actual_value = notification_database_api.get_by_id(random_notification.id)
            expected_value = random_notification
            assert actual_value == expected_value

    def test_should_add_correct_notification(
        self, app, seed_tables_with_random_data, truncate_database
    ):
        with app.app_context():
            random_task = Task.query.order_by(db.func.random()).first()
            random_notification = create_random_notification(task_id=random_task.id)
            notification_database_api.add(random_notification)
            actual_value = Notification.query.get(random_notification.id)
            expected_value = random_notification
            assert actual_value == expected_value

    def test_should_update_correct_notification(
        self, app, seed_tables_with_random_data, truncate_database
    ):
        with app.app_context():
            old_notification = Notification.query.order_by(db.func.random()).first()
            new_notification = create_random_notification(
                task_id=old_notification.task_id
            )
            notification_database_api.update_whole_by_id(
                id=old_notification.id, item_to_put=new_notification
            )
            db.session.commit()

            updated_notification = Notification.query.get(old_notification.id)

            assert updated_notification.date_and_time == new_notification.date_and_time
            assert updated_notification.task_id == new_notification.task_id

    def test_should_delete_correct_notification(
        self, app, seed_tables_with_random_data, truncate_database
    ):
        with app.app_context():
            random_notification = Notification.query.order_by(db.func.random()).first()
            notification_database_api.delete_by_id(random_notification.id)
            actual_value = Notification.query.count()
            expected_value = 0
            assert actual_value == expected_value


class TestGetTemporaryToken:
    def test_should_return_correct_token(self):
        user_email = create_random_string()
        temporary_token = get_temporary_token(user_email)
        decoded_token = jwt.decode(
            temporary_token, key=SECRET_KEY, algorithms=["HS256"]
        )
        assert "token_owner" in decoded_token
        assert decoded_token["token_owner"] == user_email
        assert "exp" in decoded_token


@pytest.fixture
def mail_mock(mocker):
    return mocker.patch("app.main.mail.send")


class TestSendMail:
    def test_should_send_correct_mail(self, mail_mock):
        subject = "Test Subject"
        message_body = "Test Body"
        message_html = "<html><body>Test HTML</body></html>"
        recipients = ["recipient1@example.com", "recipient2@example.com"]

        send_mail(subject, message_body, message_html, recipients)
        mail_mock.assert_called_once()
        first_call_args, _ = mail_mock.call_args
        msg = first_call_args[0]

        assert msg.subject == subject
        assert msg.recipients == recipients
        assert msg.sender == GOOGLE_ACCOUNT
        assert msg.body == message_body
        assert msg.html == message_html

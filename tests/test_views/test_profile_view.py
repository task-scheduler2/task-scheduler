from flask import url_for
from flask_login import current_user, login_user

from app.main import user_database_api
from tests.conftest import create_random_email, create_random_string, create_random_user


class TestTemplateRendering:
    """Testing template rendering based on whether:
    - user was authenticated."""

    def test_get_should_render_profile_template_if_authenticated(self, client):
        test_user = create_random_user()
        login_user(test_user)
        response = client.get(url_for("profile"))
        assert response.status_code == 200
        assert b"profile" in response.data

    def test_get_should_render_correct_username_if_authenticated(self, client):
        test_user = create_random_user()
        login_user(test_user)
        response = client.get(url_for("profile"))
        assert response.status_code == 200
        assert bytes(current_user.name, "utf-8") in response.data

    def test_get_should_not_render_correct_username_if_not_authenticated(self, client):
        test_user = create_random_user()
        response = client.get(url_for("profile"))
        assert response.status_code != 200
        assert bytes(test_user.name, "utf-8") not in response.data

    def test_get_should_render_correct_email_if_authenticated(self, client):
        test_user = create_random_user()
        login_user(test_user)
        response = client.get(url_for("profile"))
        assert response.status_code == 200
        assert bytes(current_user.email, "utf-8") in response.data

    def test_get_should_not_render_correct_email_if_not_authenticated(self, client):
        test_user = create_random_user()
        response = client.get(url_for("profile"))
        assert response.status_code != 200
        assert bytes(test_user.email, "utf-8") not in response.data


class TestRedirects:
    """Testing redirects based on whether:
    - user was authenticated."""

    def test_get_should_redirect_to_login_if_not_authenticated(self, client):
        response = client.get(url_for("profile"))
        assert response.status_code == 302
        assert response.location == url_for("login", next=url_for("profile"))

    def test_post_should_redirect_to_login_if_not_authenticated(self, client):
        response = client.post(url_for("profile"))
        assert response.status_code == 302
        assert response.location == url_for("login", next=url_for("profile"))

    def test_put_should_redirect_to_login_if_not_authenticated(self, client):
        response = client.put(url_for("profile"))
        assert response.status_code == 302
        assert response.location == url_for("login", next=url_for("profile"))

    def test_post_should_redirect_to_profile_if_authenticated(self, client, mocker):
        mocker.patch("app.views.send_mail")
        test_user = create_random_user()
        login_user(test_user)
        response = client.post(url_for("profile"))
        assert response.status_code == 302
        assert response.location == url_for("profile")


class TestMailSending:
    """Testing password reset mail sending based on whether:
    - user was authenticated."""

    def test_post_should_call_send_mail_if_authenticated(self, client, mocker):
        fake_send_mail = mocker.patch("app.views.send_mail")
        test_user = create_random_user()
        login_user(test_user)
        client.post(url_for("profile"))
        assert fake_send_mail.call_count == 1

    def test_post_should_send_mail_to_correct_mail_if_authenticated(
        self, client, mocker
    ):
        fake_send_mail = mocker.patch("app.views.send_mail")
        test_user = create_random_user()
        login_user(test_user)
        client.post(url_for("profile"))
        call_args, call_kwargs = fake_send_mail.call_args

        actual_recipients = call_kwargs.get("recipients", [])
        expected_recipients = [current_user.email]
        assert actual_recipients == expected_recipients

    def test_post_should_not_call_send_mail_if_not_authenticated(self, client, mocker):
        fake_send_mail = mocker.patch("app.views.send_mail")
        client.post(url_for("profile"))
        assert fake_send_mail.call_count == 0


class TestProfileUpdate:
    """Testing profile update based on whether:
    - user chose email or username to update.
    - valid data was input."""

    def test_post_should_update_name_if_name_update_was_chosen(
        self, client, truncate_database
    ):
        test_user = create_random_user()
        login_user(test_user)
        user_database_api.add(test_user)
        new_name = create_random_string()
        response = client.put(url_for("profile"), json={"usernameInput": new_name})
        assert response.status_code == 200
        assert response.get_data(as_text=True) == "Success"
        updated_user = user_database_api.get_by_id(id=test_user.id)
        assert updated_user.name == new_name

    def test_post_should_update_mail_if_mail_update_was_chosen(
        self, client, truncate_database
    ):
        test_user = create_random_user()
        login_user(test_user)
        user_database_api.add(test_user)
        new_mail = create_random_email()
        response = client.put(url_for("profile"), json={"emailInput": new_mail})
        assert response.status_code == 200
        assert response.get_data(as_text=True) == "Success"
        updated_user = user_database_api.get_by_id(id=test_user.id)
        assert updated_user.email == new_mail

    def test_post_should_not_update_mail_if_mail_update_was_chosen_and_mail_is_already_used(
        self, client, truncate_database
    ):
        test_user = create_random_user()
        test_user2 = create_random_user()
        login_user(test_user)
        user_database_api.add(test_user)
        user_database_api.add(test_user2)
        new_mail = test_user2.email
        response = client.put(url_for("profile"), json={"emailInput": new_mail})
        assert response.status_code == 400
        assert response.get_data(as_text=True) == "Failure"
        updated_user = user_database_api.get_by_id(id=test_user.id)
        assert updated_user.email != new_mail


class TestInvalidMethods:
    """Testing response status codes based on whether:
    - invalid request method was used."""

    def test_delete_should_return_405(self, client):
        response = client.delete(url_for("profile"))
        assert response.status_code == 405

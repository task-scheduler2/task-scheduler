from flask import url_for


class TestTemplateRendering:
    """Testing template rendering."""

    def test_get_should_render_correct_template(self, client):
        response = client.get(url_for("forgot_password"))
        assert response.status_code == 200
        assert b"forgot_password" in response.data


class TestRedirects:
    """Testing redirects."""

    def test_post_should_redirect_to_login(self, client):
        response = client.post(url_for("forgot_password"), json={"email": "test_email"})
        assert response.status_code == 302
        assert response.location == url_for("login")


class TestMailSending:
    """Testing password reset mail sending."""

    def test_post_should_call_send_mail(self, client, mocker):
        fake_send_mail = mocker.patch("app.views.send_mail")
        client.post(url_for("forgot_password"), json={"email": "test_email"})
        assert fake_send_mail.call_count == 1

    def test_post_should_send_mail_to_correct_mail(self, client, mocker):
        fake_send_mail = mocker.patch("app.views.send_mail")
        client.post(url_for("forgot_password"), json={"email": "test_email"})
        call_args, call_kwargs = fake_send_mail.call_args

        actual_recipients = call_kwargs.get("recipients", [])
        expected_recipients = ["test_email"]
        assert actual_recipients == expected_recipients


class TestInvalidMethods:
    """Testing response status codes based on whether:
    - invalid request method was used."""

    def test_put_should_return_405(self, client):
        response = client.put(url_for("forgot_password"))
        assert response.status_code == 405

    def test_delete_should_return_405(self, client):
        response = client.delete(url_for("forgot_password"))
        assert response.status_code == 405

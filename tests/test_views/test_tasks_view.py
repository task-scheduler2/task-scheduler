from random import randint

import pytest
from flask import url_for
from flask_login import login_user

from app.main import task_database_api
from app.models import Notification, Task, User
from tests.conftest import create_random_notification, create_random_task, random_date


class TestFullSuccess:
    """Testing view when:
    - user is logged in.
    - user has tasks created already.
    - user has notifications created already.
    - user has input valid data."""

    @pytest.fixture
    def setup(self, seed_tables_with_random_data):
        self.test_user = User.query.order_by("id").first()
        self.test_task = Task.query.order_by("id").first()
        self.test_notification = Notification.query.order_by("id").first()
        login_user(self.test_user)
        self.new_task = create_random_task(self.test_user.id)
        self.new_notification = create_random_notification(self.test_task.id)

    def test_get_should_render_correct_template(self, setup, client):
        response = client.get(url_for("tasks"))
        assert response.status_code == 200
        assert b"tasks" in response.data

    def test_get_should_have_tasks_in_its_response_data(self, setup, client):
        response = client.get(url_for("tasks"))
        expected_value = self.test_task.name
        assert response.status_code == 200
        assert expected_value.encode("utf-8") in response.data

    def test_post_should_redirect_to_tasks(self, setup, client):
        response = client.post(
            url_for("tasks"),
            json={
                "name": self.new_task.name,
                "note": self.new_task.note,
                "date_and_time": self.new_task.date_and_time,
            },
        )
        assert response.status_code == 302
        assert response.location == url_for("tasks")

    def test_post_should_create_new_task(self, setup, client):
        assert Task.query.count() == 1
        response = client.post(
            url_for("tasks"),
            json={
                "name": self.new_task.name,
                "note": self.new_task.note,
                "date_and_time": self.new_task.date_and_time,
            },
        )

        assert response.status_code == 302
        assert Task.query.count() == 2

    def test_post_should_create_new_notification_when_needed(self, setup, client):
        assert Notification.query.count() == 1
        response = client.post(
            url_for("tasks"),
            json={
                "name": self.new_task.name,
                "note": self.new_task.note,
                "date_and_time": self.new_task.date_and_time,
                "notification_1": random_date(),
            },
        )

        assert response.status_code == 302
        assert Notification.query.count() == 2

    def test_post_should_not_create_new_notification_when_not_needed(
        self, setup, client
    ):
        assert Notification.query.count() == 1
        response = client.post(
            url_for("tasks"),
            json={
                "name": self.new_task.name,
                "note": self.new_task.note,
                "date_and_time": self.new_task.date_and_time,
            },
        )

        assert response.status_code == 302
        assert Notification.query.count() == 1

    def test_post_should_create_multiple_new_notifications_when_needed(
        self, setup, client
    ):
        assert Notification.query.count() == 1
        body = {
            "name": self.new_task.name,
            "note": self.new_task.note,
            "date_and_time": self.new_task.date_and_time,
        }
        random_number = randint(2, 100)
        for number in range(1, random_number):
            notification = "notification_" + str(number)
            body[notification] = random_date()

        response = client.post(url_for("tasks"), json=body)

        assert response.status_code == 302
        assert Notification.query.count() == random_number

    def test_put_should_return_status_code_200(self, setup, client):
        task_id = self.test_task.id
        response = client.put(
            url_for("tasks-item", id=task_id),
            json={
                "name": self.new_task.name,
                "note": self.new_task.note,
                "date_and_time": self.new_task.date_and_time,
            },
        )

        assert response.status_code == 200
        assert response.get_data(as_text=True) == "Success"

    def test_put_should_replace_correct_data(self, setup, client):
        task_id = self.test_task.id
        response = client.put(
            url_for("tasks-item", id=task_id),
            json={
                "name": self.new_task.name,
                "note": self.new_task.note,
                "date_and_time": self.new_task.date_and_time,
            },
        )
        known_task = task_database_api.get_by_id(task_id)
        assert response.status_code == 200
        assert known_task.name == self.new_task.name
        assert known_task.note == self.new_task.note
        assert known_task.date_and_time == self.new_task.date_and_time.replace(
            microsecond=0
        )

    def test_delete_should_return_status_code_200(self, setup, client):
        task_id = self.test_task.id
        response = client.delete(url_for("tasks-item", id=task_id))

        assert response.status_code == 200
        assert response.get_data(as_text=True) == "Success"

    def test_delete_should_delete_single_task(self, setup, client):
        task_id = self.test_task.id
        task_database_api.add(self.new_task)
        assert Task.query.count() == 2
        response = client.delete(url_for("tasks-item", id=task_id))
        assert response.status_code == 200
        assert Task.query.count() == 1

    def test_delete_should_delete_correct_task(self, setup, client):
        task_id = self.test_task.id
        task_database_api.add(self.new_task)
        response = client.delete(url_for("tasks-item", id=task_id))
        task_database_api.get_by_id(task_id)
        assert response.status_code == 200
        assert task_database_api.get_by_id(task_id) is None
        assert task_database_api.get_by_id(self.new_task.id) is not None


class TestNotLoggedIn:
    """Testing view when:
    - user is not logged in.
    - user has tasks created already.
    - user has notifications created already.
    - user has input valid data."""

    @pytest.fixture
    def setup(self, seed_tables_with_random_data):
        self.test_user = User.query.order_by("id").first()
        self.test_task = Task.query.order_by("id").first()
        self.test_notification = Notification.query.order_by("id").first()
        self.new_task = create_random_task(self.test_user.id)
        self.new_notification = create_random_notification(self.test_task.id)

    def test_get_should_redirect_to_login(self, setup, client):
        response = client.get(url_for("tasks"))
        assert response.status_code == 302
        assert response.location == url_for("login", next=url_for("tasks"))

    def test_get_should_not_have_tasks_in_its_response_data(self, setup, client):
        response = client.get(url_for("tasks"))
        expected_value = self.test_task.name
        assert expected_value.encode("utf-8") not in response.data

    def test_post_should_redirect_to_login(self, setup, client):
        response = client.post(
            url_for("tasks"),
            json={
                "name": self.new_task.name,
                "note": self.new_task.note,
                "date_and_time": self.new_task.date_and_time,
            },
        )
        assert response.status_code == 302
        assert response.location == url_for("login", next=url_for("tasks"))

    def test_post_should_not_create_new_task(self, setup, client):
        assert Task.query.count() == 1
        response = client.post(
            url_for("tasks"),
            json={
                "name": self.new_task.name,
                "note": self.new_task.note,
                "date_and_time": self.new_task.date_and_time,
            },
        )

        assert response.status_code == 302
        assert Task.query.count() == 1

    def test_post_should_not_create_new_notification(self, setup, client):
        assert Notification.query.count() == 1
        response = client.post(
            url_for("tasks"),
            json={
                "name": self.new_task.name,
                "note": self.new_task.note,
                "date_and_time": self.new_task.date_and_time,
                "notification_1": random_date(),
            },
        )

        assert response.status_code == 302
        assert Notification.query.count() == 1

    def test_post_should_not_create_multiple_new_notifications(self, setup, client):
        assert Notification.query.count() == 1
        body = {
            "name": self.new_task.name,
            "note": self.new_task.note,
            "date_and_time": self.new_task.date_and_time,
        }
        random_number = randint(2, 100)
        for number in range(1, random_number):
            notification = "notification_" + str(number)
            body[notification] = random_date()

        response = client.post(url_for("tasks"), json=body)

        assert response.status_code == 302
        assert Notification.query.count() == 1

    def test_put_should_redirect_to_login(self, setup, client):
        task_id = self.test_task.id
        response = client.put(
            url_for("tasks-item", id=task_id),
            json={
                "name": self.new_task.name,
                "note": self.new_task.note,
                "date_and_time": self.new_task.date_and_time,
            },
        )

        assert response.status_code == 302
        assert response.location == url_for(
            "login", next=url_for("tasks-item", id=task_id)
        )

    def test_put_should_not_replace_data(self, setup, client):
        task_id = self.test_task.id
        client.put(
            url_for("tasks-item", id=task_id),
            json={
                "name": self.new_task.name,
                "note": self.new_task.note,
                "date_and_time": self.new_task.date_and_time,
            },
        )
        known_task = task_database_api.get_by_id(task_id)
        assert known_task.name != self.new_task.name
        assert known_task.note != self.new_task.note
        assert known_task.date_and_time != self.new_task.date_and_time.replace(
            microsecond=0
        )

    def test_delete_should_redirect_to_login(self, setup, client):
        task_id = self.test_task.id
        response = client.delete(url_for("tasks-item", id=task_id))

        assert response.status_code == 302
        assert response.location == url_for(
            "login", next=url_for("tasks-item", id=task_id)
        )

    def test_delete_should_not_delete_single_task(self, setup, client):
        task_id = self.test_task.id
        task_database_api.add(self.new_task)
        assert Task.query.count() == 2
        client.delete(url_for("tasks-item", id=task_id))
        assert Task.query.count() == 2


class TestNoTasks:
    """Testing view when:
    - user is logged in.
    - user does not have tasks nor notifications created.
    - user has input valid data."""

    @pytest.fixture
    def setup(self, seed_tables_with_random_data):
        self.test_user = User.query.order_by("id").first()
        login_user(self.test_user)
        self.new_task = create_random_task(self.test_user.id)

    def test_get_should_render_correct_template(self, setup, client):
        response = client.get(url_for("tasks"))
        assert response.status_code == 200
        assert b"tasks" in response.data

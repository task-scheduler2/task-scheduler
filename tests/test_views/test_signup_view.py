from flask import url_for
from flask_login import current_user, login_user
from werkzeug.security import check_password_hash

from app.main import user_database_api
from app.models import User
from tests.conftest import create_random_user


class TestTemplateRendering:
    """Testing template rendering based on whether:
    - user is authenticated."""

    def test_get_should_render_signup_template_if_user_not_authenticated(self, client):
        response = client.get(url_for("signup"))
        assert response.status_code == 200
        assert b"signup" in response.data

    def test_get_should_render_logged_template_if_user_is_authenticated(
        self, client, truncate_database
    ):
        test_user = create_random_user()
        login_user(test_user)
        response = client.get(url_for("signup"))
        assert response.status_code == 200
        assert b"logged" in response.data


class TestLogin:
    """Testing login based on whether:
    - user already exists.
    - valid data was submitted."""

    def test_post_should_login_user_if_valid_data_was_input_and_user_not_in_database(
        self, client, truncate_database
    ):
        new_user = create_random_user()
        client.post(
            url_for("signup"),
            json={
                "name": new_user.name,
                "email": new_user.email,
                "password": new_user.password,
            },
        )
        assert current_user.is_authenticated is True

    def test_post_should_not_login_user_if_valid_data_was_input_and_user_already_in_database(
        self, client, truncate_database
    ):
        new_user = create_random_user()
        user_database_api.add(new_user)
        client.post(
            url_for("signup"),
            json={
                "name": new_user.name,
                "email": new_user.email,
                "password": new_user.password,
            },
        )
        assert current_user.is_authenticated is False


class TestSignup:
    """Testing signup based on whether:
    - user already exists.
    - valid data was submitted."""

    def test_post_should_signup_user_if_valid_data_was_input_and_user_not_in_database(
        self, client, truncate_database
    ):
        assert User.query.count() == 0
        new_user = create_random_user()
        client.post(
            url_for("signup"),
            json={
                "name": new_user.name,
                "email": new_user.email,
                "password": new_user.password,
            },
        )
        known_user = User.query.filter_by(email=new_user.email).first()
        assert User.query.count() == 1
        assert known_user.name == new_user.name
        assert known_user.email == new_user.email
        assert check_password_hash(known_user.password, new_user.password) is True

    def test_post_should_not_signup_user_if_valid_data_was_input_and_user_in_database(
        self, client, truncate_database
    ):
        new_user = create_random_user()
        user_database_api.add(new_user)
        assert User.query.count() == 1
        client.post(
            url_for("signup"),
            json={
                "name": new_user.name,
                "email": new_user.email,
                "password": new_user.password,
            },
        )
        assert User.query.count() == 1


class TestRedirects:
    """Testing signup based on whether:
    - user already exists.
    - valid data was submitted."""

    def test_post_should_redirect_to_home_if_signup_successful(
        self, client, truncate_database
    ):
        new_user = create_random_user()
        response = client.post(
            url_for("signup"),
            json={
                "name": new_user.name,
                "email": new_user.email,
                "password": new_user.password,
            },
        )
        assert User.query.count() == 1
        assert response.status_code == 302
        assert response.location == url_for("home")

    def test_post_should_redirect_to_signup_if_signup_unsuccessful(
        self, client, truncate_database
    ):
        new_user = create_random_user()
        user_database_api.add(new_user)
        response = client.post(
            url_for("signup"),
            json={
                "name": new_user.name,
                "email": new_user.email,
                "password": new_user.password,
            },
        )
        assert User.query.count() == 1
        assert response.status_code == 302
        assert response.location == url_for("signup")


class TestInvalidMethods:
    """Testing response status codes based on whether:
    - invalid request method was used."""

    def test_put_should_return_405(self, client):
        response = client.put(url_for("signup"))
        assert response.status_code == 405

    def test_delete_should_return_405(self, client):
        response = client.delete(url_for("signup"))
        assert response.status_code == 405

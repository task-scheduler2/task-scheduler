from unittest.mock import MagicMock

from flask import url_for
from flask_login import current_user

from app.main import user_database_api
from app.models import User


class TestLogin:
    """Testing login based on whether:
    - user was verified by authentication vendor."""

    def test_get_should_login_user_if_verified(self, mocker, client, truncate_database):
        mocker.patch(
            "requests.get",
            return_value=MagicMock(
                json=lambda: {
                    "token_endpoint": "fake_token_endpoint",
                    "userinfo_endpoint": "fake_userinfo_endpoint",
                    "email_verified": True,
                    "email": "test@example.com",
                    "given_name": "TestUser",
                    "picture": "fake_picture",
                }
            ),
        )
        mocker.patch(
            "requests.post",
            return_value=MagicMock(json=lambda: {"access_token": "fake_access_token"}),
        )

        assert current_user.is_authenticated is False
        client.get(url_for("callback", code="test_code"))

        assert current_user.is_authenticated is True

    def test_get_should_not_login_user_if_not_verified(
        self, mocker, client, truncate_database
    ):
        mocker.patch(
            "requests.get",
            return_value=MagicMock(
                json=lambda: {
                    "token_endpoint": "fake_token_endpoint",
                    "userinfo_endpoint": "fake_userinfo_endpoint",
                    "email_verified": False,
                    "email": "test@example.com",
                    "given_name": "TestUser",
                    "picture": "fake_picture",
                }
            ),
        )
        mocker.patch(
            "requests.post",
            return_value=MagicMock(json=lambda: {"access_token": "fake_access_token"}),
        )

        assert current_user.is_authenticated is False
        client.get(url_for("callback", code="test_code"))

        assert current_user.is_authenticated is False


class TestSignup:
    """Testing signup based on whether:
    - user was verified by authentication vendor.
    - user exists already."""

    def test_get_should_signup_user_if_email_not_in_database_and_user_is_verified(
        self, mocker, client, truncate_database
    ):
        mocker.patch(
            "requests.get",
            return_value=MagicMock(
                json=lambda: {
                    "token_endpoint": "fake_token_endpoint",
                    "userinfo_endpoint": "fake_userinfo_endpoint",
                    "email_verified": True,
                    "email": "test@example.com",
                    "given_name": "TestUser",
                    "picture": "fake_picture",
                }
            ),
        )
        mocker.patch(
            "requests.post",
            return_value=MagicMock(json=lambda: {"access_token": "fake_access_token"}),
        )

        assert User.query.count() == 0
        client.get(url_for("callback", code="test_code"))

        assert User.query.count() == 1

    def test_get_should_not_signup_user_if_email_in_database_and_user_is_verified(
        self, mocker, client, truncate_database
    ):
        mocker.patch(
            "requests.get",
            return_value=MagicMock(
                json=lambda: {
                    "token_endpoint": "fake_token_endpoint",
                    "userinfo_endpoint": "fake_userinfo_endpoint",
                    "email_verified": True,
                    "email": "test@example.com",
                    "given_name": "TestUser",
                    "picture": "fake_picture",
                }
            ),
        )
        mocker.patch(
            "requests.post",
            return_value=MagicMock(json=lambda: {"access_token": "fake_access_token"}),
        )
        new_user = User(name="TestUser", email="test@example.com", password="google")
        user_database_api.add(new_user)

        assert User.query.count() == 1
        client.get(url_for("callback", code="test_code"))

        assert User.query.count() == 1

    def test_get_should_not_signup_user_if_email_in_database_and_user_is_not_verified(
        self, mocker, client, truncate_database
    ):
        mocker.patch(
            "requests.get",
            return_value=MagicMock(
                json=lambda: {
                    "token_endpoint": "fake_token_endpoint",
                    "userinfo_endpoint": "fake_userinfo_endpoint",
                    "email_verified": False,
                    "email": "test@example.com",
                    "given_name": "TestUser",
                    "picture": "fake_picture",
                }
            ),
        )
        mocker.patch(
            "requests.post",
            return_value=MagicMock(json=lambda: {"access_token": "fake_access_token"}),
        )
        new_user = User(name="TestUser", email="test@example.com", password="google")
        user_database_api.add(new_user)

        assert User.query.count() == 1
        client.get(url_for("callback", code="test_code"))

        assert User.query.count() == 1

    def test_get_should_not_signup_user_if_email_not_in_database_and_user_is_not_verified(
        self, mocker, client, truncate_database
    ):
        mocker.patch(
            "requests.get",
            return_value=MagicMock(
                json=lambda: {
                    "token_endpoint": "fake_token_endpoint",
                    "userinfo_endpoint": "fake_userinfo_endpoint",
                    "email_verified": False,
                    "email": "test@example.com",
                    "given_name": "TestUser",
                    "picture": "fake_picture",
                }
            ),
        )
        mocker.patch(
            "requests.post",
            return_value=MagicMock(json=lambda: {"access_token": "fake_access_token"}),
        )

        assert User.query.count() == 0
        client.get(url_for("callback", code="test_code"))

        assert User.query.count() == 0


class TestRedirects:
    """Testing redirects based on whether:
    - user was verified by authentication vendor."""

    def test_get_should_redirect_user_to_login_page_if_not_verified(
        self, mocker, client, truncate_database
    ):
        mocker.patch(
            "requests.get",
            return_value=MagicMock(
                json=lambda: {
                    "token_endpoint": "fake_token_endpoint",
                    "userinfo_endpoint": "fake_userinfo_endpoint",
                    "email_verified": False,
                    "email": "test@example.com",
                    "given_name": "TestUser",
                    "picture": "fake_picture",
                }
            ),
        )
        mocker.patch(
            "requests.post",
            return_value=MagicMock(json=lambda: {"access_token": "fake_access_token"}),
        )

        response = client.get(url_for("callback", code="test_code"))
        assert response.status_code == 302
        assert response.location == url_for("login")

    def test_get_should_redirect_user_to_home_page_if_verified(
        self, mocker, client, truncate_database
    ):
        mocker.patch(
            "requests.get",
            return_value=MagicMock(
                json=lambda: {
                    "token_endpoint": "fake_token_endpoint",
                    "userinfo_endpoint": "fake_userinfo_endpoint",
                    "email_verified": True,
                    "email": "test@example.com",
                    "given_name": "TestUser",
                    "picture": "fake_picture",
                }
            ),
        )
        mocker.patch(
            "requests.post",
            return_value=MagicMock(json=lambda: {"access_token": "fake_access_token"}),
        )

        response = client.get(url_for("callback", code="test_code"))
        assert response.status_code == 302
        assert response.location == url_for("home")


class TestInvalidMethods:
    """Testing response status codes based on whether:
    - invalid request method was used."""

    def test_post_should_return_405(self, client):
        response = client.post(url_for("callback"))
        assert response.status_code == 405

    def test_put_should_return_405(self, client):
        response = client.put(url_for("callback"))
        assert response.status_code == 405

    def test_delete_should_return_405(self, client):
        response = client.delete(url_for("callback"))
        assert response.status_code == 405

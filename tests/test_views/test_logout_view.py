import pytest
from flask import url_for
from flask_login import current_user, login_user

from tests.conftest import create_random_user


class TestUserLoggedIn:
    """Testing view when:
    - user is logged in."""

    @pytest.fixture
    def setup(self):
        self.test_user = create_random_user()
        login_user(self.test_user)

    def test_get_should_logout_user(self, setup, client):
        assert current_user.is_authenticated is True
        client.get(url_for("logout"))
        assert current_user.is_authenticated is False

    def test_get_should_redirect_to_login(self, setup, client):
        response = client.get(url_for("logout"))
        assert response.status_code == 302
        assert response.location == url_for("login")

    def test_post_should_return_405(self, setup, client):
        response = client.post(url_for("logout"))
        assert response.status_code == 405

    def test_put_should_return_405(self, setup, client):
        response = client.put(url_for("logout"))
        assert response.status_code == 405

    def test_delete_should_return_405(self, setup, client):
        response = client.delete(url_for("logout"))
        assert response.status_code == 405


class TestUserLoggedOut:
    """Testing view when:
    - user is not logged in."""

    @pytest.fixture
    def setup(self):
        self.test_user = create_random_user()

    def test_get_should_not_logout_user(self, setup, client):
        assert current_user.is_authenticated is False
        client.get(url_for("logout"))
        assert current_user.is_authenticated is False

    def test_get_should_redirect_to_login(self, setup, client):
        response = client.get(url_for("logout"))
        assert response.status_code == 302
        assert response.location == url_for("login", next=url_for("logout"))

    def test_post_should_return_405(self, setup, client):
        response = client.post(url_for("logout"))
        assert response.status_code == 405

    def test_put_should_return_405(self, setup, client):
        response = client.put(url_for("logout"))
        assert response.status_code == 405

    def test_delete_should_return_405(self, setup, client):
        response = client.delete(url_for("logout"))
        assert response.status_code == 405

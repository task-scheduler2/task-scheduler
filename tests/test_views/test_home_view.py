from flask import url_for


class TestTemplateRendering:
    """Testing template rendering"""

    def test_get_should_render_home_template(self, client):
        response = client.get(url_for("home"))
        assert response.status_code == 200
        assert b"home" in response.data


class TestInvalidMethods:
    """Testing response status codes based on whether:
    - invalid request method was used."""

    def test_post_should_return_405(self, client):
        response = client.post(url_for("home"))
        assert response.status_code == 405

    def test_put_should_return_405(self, client):
        response = client.put(url_for("home"))
        assert response.status_code == 405

    def test_delete_should_return_405(self, client):
        response = client.delete(url_for("home"))
        assert response.status_code == 405

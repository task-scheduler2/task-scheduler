from flask import url_for
from flask_login import current_user, login_user
from werkzeug.security import check_password_hash, generate_password_hash

from app.main import user_database_api
from tests.conftest import create_random_user


class TestTemplateRendering:
    """Testing template rendering based on whether:
    - user is authenticated."""

    def test_get_should_return_login_template_if_user_is_not_authenticated(
        self, client
    ):
        response = client.get(url_for("login"))
        assert response.status_code == 200
        assert b"login" in response.data

    def test_get_should_return_correct_template_if_user_is_authenticated(self, client):
        test_user = create_random_user()
        login_user(test_user)
        response = client.get(url_for("login"))
        assert response.status_code == 200
        assert b"logged" in response.data


class TestRedirects:
    """Testing redirects based on whether:
    - user hasn't set up a password.
    - user exists already.
    - password hash matches."""

    def test_post_should_redirect_to_google_if_user_email_in_database_and_no_password_was_set(
        self, client, truncate_database
    ):
        test_user = create_random_user()
        test_user.password = "google"
        user_database_api.add(test_user)
        response = client.post(
            url_for("login"),
            json={"email": test_user.email, "password": test_user.password},
        )
        assert response.status_code == 302
        assert response.location == url_for("google")

    def test_post_should_redirect_back_to_login_if_user_email_not_in_database(
        self, client
    ):
        test_user = create_random_user()
        response = client.post(
            url_for("login"),
            json={"email": test_user.email, "password": test_user.password},
        )
        assert response.status_code == 302
        assert response.location == url_for("login")

    def test_post_should_redirect_to_tasks_if_email_in_database_and_password_hash_matches(
        self, client, truncate_database
    ):
        test_user = create_random_user()
        test_password = test_user.password
        test_user.password = generate_password_hash(test_user.password, method="sha256")
        user_database_api.add(test_user)
        response = client.post(
            url_for("login"), json={"email": test_user.email, "password": test_password}
        )
        assert response.status_code == 302
        assert check_password_hash(test_user.password, test_password)
        assert response.location == url_for("tasks")

    def test_post_should_redirect_to_login_if_email_in_database_and_password_hash_does_not_match(
        self, client, truncate_database
    ):
        test_user = create_random_user()
        test_user.password = generate_password_hash(test_user.password, method="sha256")
        test_password = test_user.password + "wrong"
        user_database_api.add(test_user)
        response = client.post(
            url_for("login"), json={"email": test_user.email, "password": test_password}
        )
        assert response.status_code == 302
        assert response.location == url_for("login")


class TestLogin:
    """Testing redirects based on whether:
    - user exists already.
    - password hash matches."""

    def test_post_should_login_user_if_email_in_database_and_password_hash_matches(
        self, client, truncate_database
    ):
        assert current_user.is_authenticated is False
        test_user = create_random_user()
        test_password = test_user.password
        test_user.password = generate_password_hash(test_user.password, method="sha256")
        user_database_api.add(test_user)
        client.post(
            url_for("login"), json={"email": test_user.email, "password": test_password}
        )
        assert current_user.is_authenticated is True

    def test_post_should_not_login_user_if_email_in_database_and_password_hash_does_not_match(
        self, client, truncate_database
    ):
        assert current_user.is_authenticated is False
        test_user = create_random_user()
        test_user.password = generate_password_hash(test_user.password, method="sha256")
        user_database_api.add(test_user)
        test_password = test_user.password + "wrong"
        client.post(
            url_for("login"), json={"email": test_user.email, "password": test_password}
        )
        assert current_user.is_authenticated is False

    def test_post_should_not_login_user_if_email_not_in_database(
        self, client, truncate_database
    ):
        assert current_user.is_authenticated is False
        test_user = create_random_user()
        test_password = test_user.password
        test_user.password = generate_password_hash(test_user.password, method="sha256")
        test_email = test_user.email + "wrong"
        user_database_api.add(test_user)
        client.post(
            url_for("login"), json={"email": test_email, "password": test_password}
        )
        assert current_user.is_authenticated is False


class TestInvalidMethods:
    """Testing response status codes based on whether:
    - invalid request method was used."""

    def test_put_should_return_405(self, client):
        response = client.put(url_for("login"))
        assert response.status_code == 405

    def test_delete_should_return_405(self, client):
        response = client.delete(url_for("login"))
        assert response.status_code == 405

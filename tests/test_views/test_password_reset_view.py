from time import time

import jwt
from flask import url_for
from flask_login import current_user, login_user
from werkzeug.security import check_password_hash

from app.main import get_temporary_token, user_database_api
from app.models import User
from tests.conftest import create_random_user


class TestTemplateRendering:
    """Testing template rendering based on whether:
    - token is valid.
    - user exists."""

    def test_get_should_render_correct_template_if_token_is_valid_and_user_exists(
        self, client, truncate_database
    ):
        test_user = create_random_user()
        user_database_api.add(test_user)
        token = get_temporary_token(test_user.email)
        response = client.get(url_for("password_reset", token=token))
        assert response.status_code == 200
        assert b"password_reset" in response.data

    def test_get_should_not_render_correct_template_if_token_is_expired_and_user_exists(
        self, app, client, truncate_database
    ):
        test_user = create_random_user()
        user_database_api.add(test_user)
        token = jwt.encode(
            {"token_owner": test_user.email, "exp": time() - 500}, key=app.secret_key
        )
        response = client.get(url_for("password_reset", token=token))
        assert response.status_code == 400
        assert b"password_reset" not in response.data
        assert response.get_data(as_text=True) == "Token expired"

    def test_get_should_render_correct_template_if_token_is_valid_and_user_does_not_exist(
        self, client
    ):
        test_user = create_random_user()
        token = get_temporary_token(test_user.email)
        response = client.get(url_for("password_reset", token=token))
        assert response.status_code == 400
        assert b"password_reset" not in response.data
        assert response.get_data(as_text=True) == "Invalid user"


class TestRedirects:
    """Testing template rendering based on whether:
    - token is valid.
    - user exists."""

    def test_post_should_redirect_to_home_if_token_is_valid_and_user_exists(
        self, client, truncate_database
    ):
        test_user = create_random_user()
        user_database_api.add(test_user)
        token = get_temporary_token(test_user.email)
        response = client.post(
            url_for("password_reset", token=token), json={"password": "test_password"}
        )
        assert response.status_code == 302
        assert response.location == url_for("home")

    def test_post_should_not_redirect_to_home_if_token_is_invalid_and_user_exists(
        self, app, client, truncate_database
    ):
        test_user = create_random_user()
        user_database_api.add(test_user)
        token = jwt.encode(
            {"token_owner": test_user.email, "exp": time() - 500}, key=app.secret_key
        )
        response = client.post(
            url_for("password_reset", token=token), json={"password": "test_password"}
        )
        assert response.status_code == 400
        assert response.get_data(as_text=True) == "Token expired"

    def test_post_should_redirect_to_home_if_token_is_valid_and_user_does_not_exist(
        self, client
    ):
        test_user = create_random_user()
        token = get_temporary_token(test_user.email)
        response = client.post(
            url_for("password_reset", token=token), json={"password": "test_password"}
        )
        assert response.status_code == 400
        assert response.get_data(as_text=True) == "Invalid user"


class TestPasswordUpdate:
    """Testing password update based on whether:
    - token is valid.
    - user exists.
    - user input valid password."""

    def test_post_should_update_password_if_token_is_valid_and_user_exists(
        self, client, truncate_database
    ):
        test_user = create_random_user()
        user_database_api.add(test_user)
        token = get_temporary_token(test_user.email)
        response = client.post(
            url_for("password_reset", token=token), json={"password": "test_password"}
        )
        known_user = (
            User.query.filter_by(email=test_user.email).order_by("email").first()
        )
        assert response.status_code == 302
        assert check_password_hash(known_user.password, "test_password")

    def test_post_should_not_update_password_if_token_expired_and_user_exists(
        self, app, client, truncate_database
    ):
        test_user = create_random_user()
        user_database_api.add(test_user)
        token = jwt.encode(
            {"token_owner": test_user.email, "exp": time() - 500}, key=app.secret_key
        )
        response = client.post(
            url_for("password_reset", token=token), json={"password": "test_password"}
        )
        known_user = (
            User.query.filter_by(email=test_user.email).order_by("email").first()
        )
        assert response.status_code == 400
        assert response.get_data(as_text=True) == "Token expired"
        assert test_user.password == known_user.password

    def test_post_should_not_update_password_if_token_is_valid_and_user_does_not_exist(
        self, client
    ):
        test_user = create_random_user()
        token = get_temporary_token(test_user.email)
        response = client.post(
            url_for("password_reset", token=token), json={"password": "test_password"}
        )
        assert response.status_code == 400
        assert response.get_data(as_text=True) == "Invalid user"
        assert User.query.count() == 0


class TestLogout:
    """Testing logout based on whether:
    - token is valid.
    - user exists.
    - user input valid password."""

    def test_post_should_logout_user_if_token_is_valid_and_user_exists(
        self, client, truncate_database
    ):
        test_user = create_random_user()
        login_user(test_user)
        user_database_api.add(test_user)
        token = get_temporary_token(test_user.email)
        response = client.post(
            url_for("password_reset", token=token), json={"password": "test_password"}
        )
        assert response.status_code == 302
        assert current_user.is_authenticated is False

    def test_post_should_not_logout_user_if_token_expired_and_user_exists(
        self, app, client, truncate_database
    ):
        test_user = create_random_user()
        user_database_api.add(test_user)
        login_user(test_user)
        token = jwt.encode(
            {"token_owner": test_user.email, "exp": time() - 500}, key=app.secret_key
        )
        response = client.post(
            url_for("password_reset", token=token), json={"password": "test_password"}
        )
        assert response.status_code == 400
        assert response.get_data(as_text=True) == "Token expired"
        assert current_user.is_authenticated is True

    def test_post_should_not_update_password_if_token_is_valid_and_user_does_not_exist(
        self, client
    ):
        test_user = create_random_user()
        login_user(test_user)
        token = get_temporary_token(test_user.email)
        response = client.post(
            url_for("password_reset", token=token), json={"password": "test_password"}
        )
        assert response.status_code == 400
        assert response.get_data(as_text=True) == "Invalid user"
        assert current_user.is_authenticated is True


class TestInvalidMethods:
    """Testing response status codes based on whether:
    - invalid request method was used."""

    def test_put_should_return_405(self, client):
        response = client.put(url_for("password_reset", token="token"))
        assert response.status_code == 405

    def test_delete_should_return_405(self, client):
        response = client.delete(url_for("password_reset", token="token"))
        assert response.status_code == 405

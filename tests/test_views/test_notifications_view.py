import pytest
from flask import url_for
from flask_login import login_user

from app.main import notification_database_api
from app.models import Notification, Task, User
from tests.conftest import create_random_notification, create_random_task, random_date


class TestFullSuccess:
    """Testing view when:
    - user is logged in.
    - user has tasks created already.
    - user has notifications created already.
    - user has input valid data."""

    @pytest.fixture(autouse=True)
    def setup(self, seed_tables_with_random_data):
        self.test_user = User.query.order_by("id").first()
        self.test_task = Task.query.order_by("id").first()
        self.test_notification = Notification.query.order_by("id").first()
        login_user(self.test_user)
        self.new_task = create_random_task(self.test_user.id)
        self.new_notification = create_random_notification(self.test_task.id)

    def test_get_should_return_status_code_405(self, client):
        response = client.get(url_for("notifications", id=self.test_task.id))
        assert response.status_code == 405

    def test_post_should_return_status_code_200(self, client):
        response = client.post(
            url_for("notifications", id=self.test_task.id),
            json={"newInputValue": random_date()},
        )
        assert response.status_code == 200
        assert response.get_data(as_text=True) == "Success"

    def test_post_should_create_new_notification(self, client):
        response = client.post(
            url_for("notifications", id=self.test_task.id),
            json={"newInputValue": random_date()},
        )
        assert response.status_code == 200
        assert Notification.query.count() == 2

    def test_put_should_return_status_code_200(self, client):
        response = client.put(
            url_for("notifications", id=self.test_notification.id),
            json={"newInputValue": random_date()},
        )
        assert response.status_code == 200
        assert response.get_data(as_text=True) == "Success"

    def test_put_should_not_modify_number_of_notifications(self, client):
        response = client.put(
            url_for("notifications", id=self.test_notification.id),
            json={"newInputValue": random_date()},
        )
        assert response.status_code == 200
        assert Notification.query.count() == 1

    def test_put_should_update_correct_notification(self, client):
        notification_id = self.test_notification.id
        new_date = random_date()
        response = client.put(
            url_for("notifications", id=notification_id),
            json={"newInputValue": new_date},
        )
        known_notification = notification_database_api.get_by_id(notification_id)
        assert response.status_code == 200
        assert known_notification.date_and_time == new_date.replace(microsecond=0)

    def test_delete_should_return_status_code_200(self, client):
        notification_id = self.test_notification.id
        response = client.delete(url_for("notifications", id=notification_id))
        assert response.status_code == 200
        assert response.get_data(as_text=True) == "Success"

    def test_delete_should_delete_single_notification(self, client):
        notification_id = self.test_notification.id
        response = client.delete(url_for("notifications", id=notification_id))
        assert response.status_code == 200
        assert Notification.query.count() == 0

    def test_delete_should_delete_correct_notification(self, client):
        notification_id = self.test_notification.id
        notification_database_api.add(self.new_notification)
        response = client.delete(url_for("notifications", id=notification_id))
        known_notification = notification_database_api.get_by_id(notification_id)
        assert response.status_code == 200
        assert known_notification is None


class TestNotLoggedIn:
    """Testing view when:
    - user is not logged in.
    - user has tasks created already.
    - user has notifications created already.
    - user has input valid data."""

    @pytest.fixture(autouse=True)
    def setup(self, seed_tables_with_random_data):
        self.test_user = User.query.order_by("id").first()
        self.test_task = Task.query.order_by("id").first()
        self.test_notification = Notification.query.order_by("id").first()
        self.new_task = create_random_task(self.test_user.id)
        self.new_notification = create_random_notification(self.test_task.id)

    def test_post_should_redirect_to_login(self, client):
        response = client.post(
            url_for("notifications", id=self.test_task.id),
            json={"newInputValue": random_date()},
        )
        assert response.status_code == 302
        assert response.location == url_for(
            "login", next=url_for("notifications", id=self.test_task.id)
        )

    def test_put_should_redirect_to_login(self, client):
        response = client.put(
            url_for("notifications", id=self.test_notification.id),
            json={"newInputValue": random_date()},
        )
        assert response.status_code == 302
        assert response.location == url_for(
            "login", next=url_for("notifications", id=self.test_notification.id)
        )

    def test_delete_should_redirect_to_login(self, client):
        response = client.delete(url_for("notifications", id=self.test_notification.id))
        assert response.status_code == 302
        assert response.location == url_for(
            "login", next=url_for("notifications", id=self.test_notification.id)
        )

    def test_get_should_return_status_code_405(self, client):
        response = client.get(url_for("notifications", id=self.test_task.id))
        assert response.status_code == 405

    def test_get_should_return_status_code_405(self, client):
        response = client.get(url_for("notifications", id=self.test_task.id))
        assert response.status_code == 405

    def test_post_should_not_create_new_notification(self, client):
        client.post(
            url_for("notifications", id=self.test_task.id),
            json={"newInputValue": random_date()},
        )
        assert Notification.query.count() == 1

    def test_put_should_not_modify_number_of_notifications(self, client):
        client.put(
            url_for("notifications", id=self.test_notification.id),
            json={"newInputValue": random_date()},
        )
        assert Notification.query.count() == 1

    def test_put_should_not_update_notification(self, client):
        notification_id = self.test_notification.id
        new_date = random_date()
        client.put(
            url_for("notifications", id=notification_id),
            json={"newInputValue": new_date},
        )
        known_notification = notification_database_api.get_by_id(notification_id)
        assert known_notification.date_and_time != new_date.replace(microsecond=0)

    def test_delete_should_not_delete_any_notification(self, client):
        notification_id = self.test_notification.id
        client.delete(url_for("notifications", id=notification_id))
        assert Notification.query.count() == 1


class TestInvalidInput:
    """Testing view when:
    - user is logged in.
    - user has tasks created already.
    - user has notifications created already.
    - user has input invalid data."""

    @pytest.fixture(autouse=True)
    def setup(self, seed_tables_with_random_data):
        self.test_user = User.query.order_by("id").first()
        self.test_task = Task.query.order_by("id").first()
        self.test_notification = Notification.query.order_by("id").first()
        login_user(self.test_user)
        self.new_task = create_random_task(self.test_user.id)
        self.new_notification = create_random_notification(self.test_task.id)

    def test_get_should_return_status_code_405(self, client):
        response = client.get(url_for("notifications", id=self.test_task.id))
        assert response.status_code == 405

    def test_post_should_not_create_new_notification_when_parent_task_does_not_exist(
        self, client
    ):
        client.post(
            url_for("notifications", id=self.test_task.id + 1),
            json={"newInputValue": random_date()},
        )
        assert Notification.query.count() == 1

    def test_put_should_return_status_code_404_when_notification_does_not_exist(
        self, client
    ):
        response = client.put(
            url_for("notifications", id=self.test_notification.id + 1),
            json={"newInputValue": random_date()},
        )
        assert response.status_code == 404
        assert response.get_data(as_text=True) == "Not found"

    def test_put_should_not_modify_number_of_notifications_when_notification_does_not_exist(
        self, client
    ):
        client.put(
            url_for("notifications", id=self.test_notification.id + 1),
            json={"newInputValue": random_date()},
        )
        assert Notification.query.count() == 1

    def test_put_should_not_update_any_notification_when_chosen_notification_does_not_exist(
        self, client
    ):
        notification_id = self.test_notification.id + 1
        new_date = random_date()
        client.put(
            url_for("notifications", id=notification_id),
            json={"newInputValue": new_date},
        )
        known_notification = notification_database_api.get_by_id(notification_id - 1)
        assert known_notification.date_and_time != new_date.replace(microsecond=0)

    def test_delete_should_not_delete_notification_when_chosen_notification_does_not_exist(
        self, client
    ):
        notification_id = self.test_notification.id + 1
        client.delete(url_for("notifications", id=notification_id))
        assert Notification.query.count() == 1

    def test_delete_should_return_status_code_200(self, client):
        notification_id = self.test_notification.id + 1
        response = client.delete(url_for("notifications", id=notification_id))
        assert response.status_code == 404
        assert response.get_data(as_text=True) == "Not found"

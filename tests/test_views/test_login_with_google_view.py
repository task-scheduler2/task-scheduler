from urllib.parse import parse_qs, urlencode, urlparse

import requests
from flask import request, url_for

import app.app


class TestRedirects:
    """Testing redirects"""

    def test_get_should_redirect_to_correct_url(self, client):
        response = client.get(url_for("google"))
        authorization_endpoint = requests.get(app.app.GOOGLE_DISCOVERY_URL).json()[
            "authorization_endpoint"
        ]
        redirect_uri = request.base_url + "/callback"
        scope = ["openid", "email", "profile"]

        expected_request_uri = f"{authorization_endpoint}?{urlencode({'redirect_uri': redirect_uri, 'scope': ' '.join(scope), 'response_type': 'code'})}"

        parsed_expected = parse_qs(urlparse(expected_request_uri).query)
        parsed_actual = parse_qs(urlparse(response.location).query)

        expected_keys = set(parsed_expected.keys())
        actual_keys = set(parsed_actual.keys())

        assert response.status_code == 302
        assert expected_keys - {"client_id"} == actual_keys - {"client_id"}


class TestInvalidMethods:
    """Testing response status codes based on whether:
    - invalid request method was used."""

    def test_post_should_return_405(self, client):
        response = client.post(url_for("google"))
        assert response.status_code == 405

    def test_put_should_return_405(self, client):
        response = client.put(url_for("google"))
        assert response.status_code == 405

    def test_delete_should_return_405(self, client):
        response = client.delete(url_for("google"))
        assert response.status_code == 405

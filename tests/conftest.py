from datetime import datetime, timedelta
from random import choice, randint
from string import ascii_letters, digits

import pytest
from werkzeug.security import generate_password_hash

from app.app import create_app, db
from app.config import TestingConfig
from app.models import Notification, Task, User


@pytest.fixture(scope="session")
def app():
    app = create_app(config_class=TestingConfig)
    return app


@pytest.fixture()
def client(app):
    with app.test_client() as client:
        yield client


@pytest.fixture()
def runner(app):
    with app.test_cli_runner() as runner:
        return runner


@pytest.fixture(scope="function", autouse=True)
def truncate_database(app):
    """Function deleting all test database records after test is concluded."""
    with app.app_context():
        db.create_all()
        yield db
        meta = db.metadata
        for table in reversed(meta.sorted_tables):
            db.session.query(table).delete()
        db.session.commit()


@pytest.fixture(scope="function")
def seed_tables_with_random_data(app):
    """Function populating database tables with data."""
    with app.app_context():
        random_user = create_random_user()
        db.session.add(random_user)
        db.session.commit()

        random_task = create_random_task(random_user.id)
        db.session.add(random_task)
        db.session.commit()

        random_notification = create_random_notification(random_task.id)
        db.session.add(random_notification)
        db.session.commit()


def random_date():
    """Function returning a random date."""
    current_date = datetime.now()
    end_date = current_date + timedelta(days=365)
    days = (end_date - current_date).days
    random_days = randint(0, days)
    random_date = current_date + timedelta(days=random_days)

    return random_date


def create_random_string():
    """Function returning a random string."""
    length = randint(15, 50)
    characters = ascii_letters + digits
    return "".join(choice(characters) for _ in range(length))


def create_random_email():
    """Function returning a random email."""
    username_length = randint(5, 15)
    domain_length = randint(5, 15)

    username = "".join(choice(ascii_letters + digits) for _ in range(username_length))
    domain = "".join(choice(ascii_letters + digits) for _ in range(domain_length))

    return f"{username}@{domain}.com"


def create_random_user():
    """Function returning a random user."""
    random_user = User(
        name=create_random_string(),
        email=create_random_email(),
        password=generate_password_hash(create_random_string(), method="sha256"),
    )
    return random_user


def create_random_task(user_id):
    """Function returning a random task for specific user."""
    random_task = Task(
        name=create_random_string(),
        note=create_random_string(),
        user_id=user_id,
        date_and_time=random_date(),
    )
    return random_task


def create_random_notification(task_id):
    """Function returning a random notification for specific task."""
    random_notification = Notification(task_id=task_id, date_and_time=random_date())
    return random_notification

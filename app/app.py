import os

from dotenv import load_dotenv
from flask import Flask
from flask_login import LoginManager
from flask_mail import Mail
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from oauthlib.oauth2 import WebApplicationClient

from .config import DevelopmentConfig

load_dotenv()
db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()
mail = Mail()
login_manager = LoginManager()
GOOGLE_CLIENT_ID = os.environ.get("GOOGLE_CLIENT_ID")
GOOGLE_CLIENT_SECRET = os.environ.get("GOOGLE_CLIENT_SECRET")
GOOGLE_MAIL_KEY = os.environ.get("GOOGLE_MAIL_KEY")
GOOGLE_ACCOUNT = os.environ.get("GOOGLE_ACCOUNT")
GOOGLE_DISCOVERY_URL = "https://accounts.google.com/.well-known/openid-configuration"
client = WebApplicationClient(GOOGLE_CLIENT_ID)
SECRET_KEY = os.environ.get("SECRET_KEY")


def create_app(config_class=DevelopmentConfig):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)
    mail.init_app(app)
    login_manager.login_view = "login"
    login_manager.init_app(app)

    from .main import user_database_api
    from .views import (
        Callback,
        ForgotPassword,
        Home,
        Login,
        LoginWithGoogle,
        Logout,
        Notifications,
        PasswordReset,
        Profile,
        Signup,
        Tasks,
    )

    @login_manager.user_loader
    def load_user(user_id):
        return user_database_api.get_by_id(user_id)

    def register_view(app, class_view, url, view_name):
        item = class_view.as_view(view_name)
        app.add_url_rule(url, view_func=item)

    register_view(app=app, class_view=Login, url="/login", view_name="login")
    register_view(
        app=app,
        class_view=ForgotPassword,
        url="/forgot_password",
        view_name="forgot_password",
    )
    register_view(app=app, class_view=Signup, url="/signup", view_name="signup")
    register_view(app=app, class_view=Home, url="/", view_name="home")
    register_view(app=app, class_view=Profile, url="/profile", view_name="profile")
    register_view(
        app=app,
        class_view=PasswordReset,
        url="/passwordReset/<token>",
        view_name="password_reset",
    )
    register_view(app=app, class_view=Logout, url="/logout", view_name="logout")
    register_view(
        app=app, class_view=LoginWithGoogle, url="/login/google", view_name="google"
    )
    register_view(
        app=app, class_view=Callback, url="/login/google/callback", view_name="callback"
    )
    register_view(app=app, class_view=Tasks, url="/tasks", view_name="tasks")
    register_view(
        app=app, class_view=Tasks, url="/tasks/<int:id>", view_name="tasks-item"
    )
    register_view(
        app=app,
        class_view=Notifications,
        url="/notifications/<int:id>",
        view_name="notifications",
    )

    with app.app_context():
        db.create_all()

    return app

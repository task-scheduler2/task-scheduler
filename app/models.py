from __future__ import annotations

import re

from flask_login import UserMixin
from sqlalchemy.orm import validates

from .app import db, ma


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)
    email = db.Column(db.Text, nullable=False)
    password = db.Column(db.Text, nullable=False)
    tasks = db.relationship(
        "Task", backref="user", lazy=True, cascade="all, delete-orphan"
    )

    def __init__(self, name: str, email: str, password: str) -> None:
        self.name = name
        self.email = email
        self.password = password

    def update(self, modified_user: User) -> None:
        self.name = modified_user.name
        self.email = modified_user.email
        self.password = modified_user.password

    @validates("name")
    def validate_name(self, key, name):
        if not name:
            raise ValueError("Name is required")
        elif not len(name) >= 3:
            raise ValueError("Name too short")
        elif len(name) >= 100:
            raise ValueError("Name too long")
        return name

    @validates("email")
    def validate_email(self, key, email):
        if not email:
            raise ValueError("Email is required")
        elif not re.match(r"[^@]+@[^@]+\.[^@]+", email):
            raise ValueError("Invalid email address")
        elif len(email) >= 100:
            raise ValueError("Email too long")
        return email

    @validates("password")
    def validate_password(self, key, password):
        if not password:
            raise ValueError("Password is required")
        elif len(password) >= 100:
            raise ValueError("Password too long")
        return password

    @staticmethod
    def create_from_json(json_body: dict) -> User:
        return User(
            name=json_body["name"],
            email=json_body["email"],
            password=json_body["password"],
        )


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)
    note = db.Column(db.Text)
    date_and_time = db.Column(db.TIMESTAMP)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    notifications = db.relationship(
        "Notification", backref="task", lazy=True, cascade="all, delete-orphan"
    )

    def __init__(self, name: str, note: str, user_id: int, date_and_time) -> None:
        self.name = name
        self.note = note
        self.user_id = user_id
        self.date_and_time = date_and_time

    def update(self, modified_task: Task) -> None:
        self.name = modified_task.name
        self.note = modified_task.note
        self.user_id = modified_task.user_id
        self.date_and_time = modified_task.date_and_time

    @validates("name")
    def validate_name(self, key, name):
        if not name:
            raise ValueError("Name is required")
        elif not len(name) >= 3:
            raise ValueError("Name too short")
        elif len(name) >= 100:
            raise ValueError("Name too long")
        return name

    @validates("note")
    def validate_note(self, key, note):
        if len(note) >= 1000:
            raise ValueError("Note too long")
        return note

    @staticmethod
    def create_from_json(json_body: dict) -> Task:
        return Task(
            name=json_body["name"],
            note=json_body["note"],
            user_id=json_body["user_id"],
            date_and_time=json_body["date_and_time"],
        )


class Notification(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date_and_time = db.Column(db.TIMESTAMP, nullable=False)
    task_id = db.Column(db.Integer, db.ForeignKey("task.id"), nullable=False)

    def __init__(self, task_id: int, date_and_time) -> None:
        self.task_id = task_id
        self.date_and_time = date_and_time

    def update(self, modified_notification: Notification) -> None:
        self.task_id = modified_notification.task_id
        self.date_and_time = modified_notification.date_and_time

    @validates("note")
    def validate_email(self, key, note):
        if len(note) >= 1000:
            raise ValueError("Note too long")
        return note

    @staticmethod
    def create_from_json(json_body: dict) -> Notification:
        return Notification(
            task_id=json_body["task_id"],
            date_and_time=json_body["date_and_time"],
        )


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User


class TaskSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Task


class NotificationSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Notification


user_schema = UserSchema()
task_schema = TaskSchema()
notification_schema = NotificationSchema()

from time import time

import jwt
from flask_mail import Message

from .app import GOOGLE_ACCOUNT, SECRET_KEY, db, mail
from .models import (
    Notification,
    Task,
    User,
    notification_schema,
    task_schema,
    user_schema,
)


class DatabaseApi:
    def __init__(self, model, schema):
        self.model = model
        self.schema = schema

    def get_by_id(self, id: int):
        item = self.model.query.get(id)
        return item

    def add(self, item_to_post: db.Model):
        db.session.add(item_to_post)
        db.session.commit()

    def update_whole_by_id(self, id: int, item_to_put: db.Model):
        item_to_update = self.get_by_id(id)
        item_to_update.update(item_to_put)

        db.session.commit()

    def delete_by_id(self, id: int):
        item_to_delete = self.get_by_id(id)
        db.session.delete(item_to_delete)
        db.session.commit()


user_database_api = DatabaseApi(User, user_schema)
task_database_api = DatabaseApi(Task, task_schema)
notification_database_api = DatabaseApi(Notification, notification_schema)


def get_temporary_token(user_email):
    temporary_token = jwt.encode(
        {"token_owner": user_email, "exp": time() + 500}, key=SECRET_KEY
    )
    return temporary_token


def send_mail(subject: str, message_body: str, message_html, recipients: list):
    msg = Message(
        subject=subject,
        recipients=recipients,
        sender=GOOGLE_ACCOUNT,
        body=message_body,
        html=message_html,
    )
    mail.send(msg)

import json

import jwt
import requests
from flask import flash, redirect, render_template, request, url_for
from flask.views import MethodView
from flask_login import current_user, login_required, login_user, logout_user
from werkzeug.security import check_password_hash, generate_password_hash

from .app import (
    GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET,
    GOOGLE_DISCOVERY_URL,
    SECRET_KEY,
    client,
)
from .main import (
    get_temporary_token,
    notification_database_api,
    send_mail,
    task_database_api,
    user_database_api,
)
from .models import Notification, Task, User


class Login(MethodView):
    """View allowing user to log in either using email and password or google login."""

    init_every_request = False

    def get(self):
        if not current_user.is_authenticated:
            return render_template("login.html")
        else:
            return render_template("logged.html")

    def post(self):
        body = request.json
        try:
            if body["remember"]:
                remember = True
        except KeyError:
            remember = False

        known_user = User.query.filter_by(email=body.get("email")).first()
        if known_user:
            if known_user.password == "google":
                return redirect(url_for("google"))
            elif check_password_hash(known_user.password, body.get("password")):
                login_user(known_user, remember=remember)
                return redirect(url_for("tasks"))
        flash("Please check your login details and try again.")
        return redirect(url_for("login"))


class LoginWithGoogle(MethodView):
    """View redirecting user to google login."""

    init_every_request = False

    def get(self):
        authorization_endpoint = (
            requests.get(GOOGLE_DISCOVERY_URL).json().get("authorization_endpoint")
        )
        request_uri = client.prepare_request_uri(
            authorization_endpoint,
            redirect_uri=request.base_url + "/callback",
            scope=["openid", "email", "profile"],
        )
        return redirect(request_uri)


class Callback(MethodView):
    """View signing and/or logging user in if their google login passed."""

    init_every_request = False

    def get(self):
        code = request.args.get("code")
        token_endpoint = requests.get(GOOGLE_DISCOVERY_URL).json().get("token_endpoint")
        token_url, headers, body = client.prepare_token_request(
            token_endpoint,
            authorization_response=request.url,
            redirect_url=request.base_url,
            code=code,
        )
        token_response = requests.post(
            token_url,
            headers=headers,
            data=body,
            auth=(GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET),
        )

        client.parse_request_body_response(json.dumps(token_response.json()))
        userinfo_endpoint = (
            requests.get(GOOGLE_DISCOVERY_URL).json().get("userinfo_endpoint")
        )
        uri, headers, body = client.add_token(userinfo_endpoint)
        userinfo_response = requests.get(uri, headers=headers, data=body)
        if userinfo_response.json().get("email_verified"):
            user_email = userinfo_response.json().get("email")
            known_user = User.query.filter_by(email=user_email).first()
            if not known_user:
                userinfo_response.json().get("picture")
                user_name = userinfo_response.json().get("given_name")
                new_user = User(name=user_name, email=user_email, password="google")
                user_database_api.add(new_user)
                known_user = User.query.filter_by(email=user_email).first()
            login_user(known_user)

            return redirect(url_for("home"))
        flash("Please check your login details and try again.")
        return redirect(url_for("login"))


class Signup(MethodView):
    init_every_request = False

    def get(self):
        if not current_user.is_authenticated:
            return render_template("signup.html")
        else:
            return render_template("logged.html")

    def post(self):
        body = request.json
        new_user = User.create_from_json(json_body=body)
        if not User.query.filter_by(email=new_user.email).first():
            new_user.password = generate_password_hash(
                new_user.password, method="sha256"
            )
            user_database_api.add(new_user)
            known_user = User.query.filter_by(email=new_user.email).first()
            login_user(known_user)
            return redirect(url_for("home"))
        else:
            flash("Email address is already being used")

        return redirect(url_for("signup"))


class Home(MethodView):
    init_every_request = False

    def get(self):
        return render_template("index.html")


class Profile(MethodView):
    decorators = [login_required]
    init_every_request = False

    def get(self):
        return render_template(
            "profile.html", username=current_user.name, user_email=current_user.email
        )

    def post(self):
        token = get_temporary_token(current_user.email)
        link = request.host_url + url_for("password_reset", token=token)
        send_mail(
            subject="Task Scheduler password reset",
            message_body="Click the link below to reset password",
            message_html=render_template("reset_mail.html", reset_link=link),
            recipients=[current_user.email],
        )
        return redirect(url_for("profile"))

    def put(self):
        body = request.json
        for data_type, data_value in body.items():
            user = user_database_api.get_by_id(id=current_user.id)
            if data_type == "usernameInput":
                user.name = data_value
            elif data_type == "emailInput":
                if not User.query.filter_by(email=data_value).first():
                    user.email = data_value
                else:
                    return "Failure", 400
            user_database_api.update_whole_by_id(id=current_user.id, item_to_put=user)
            return "Success", 200


class ForgotPassword(MethodView):
    def get(self):
        return render_template("forgot_password.html")

    def post(self):
        body = request.json
        user_email = body.get("email")
        token = get_temporary_token(user_email)
        link = request.host_url + url_for("password_reset", token=token)
        send_mail(
            subject="Task Scheduler password reset",
            message_body="",
            message_html=render_template("reset_mail.html", reset_link=link),
            recipients=[user_email],
        )
        return redirect(url_for("login"))


class PasswordReset(MethodView):
    def get(self, token):
        try:
            user_email = jwt.decode(token, algorithms=["HS256"], key=SECRET_KEY)[
                "token_owner"
            ]
        except jwt.exceptions.ExpiredSignatureError:
            return "Token expired", 400
        user = User.query.filter_by(email=user_email).first()
        if user:
            return render_template("password_reset.html")
        return "Invalid user", 400

    def post(self, token):
        body = request.json
        try:
            user_email = jwt.decode(token, algorithms=["HS256"], key=SECRET_KEY)[
                "token_owner"
            ]
        except jwt.exceptions.ExpiredSignatureError:
            return "Token expired", 400
        user = User.query.filter_by(email=user_email).first()
        if user:
            user.password = generate_password_hash(
                body.get("password"), method="sha256"
            )
            user_database_api.update_whole_by_id(id=user.id, item_to_put=user)
            logout_user()
            return redirect(url_for("home"))
        return "Invalid user", 400


class Logout(MethodView):
    decorators = [login_required]
    init_every_request = False

    def get(self):
        logout_user()
        return redirect(url_for("login"))


class Tasks(MethodView):
    decorators = [login_required]

    def get(self):
        tasks = Task.query.filter_by(user_id=current_user.id).all()
        if tasks:
            return render_template("tasks.html", tasks=tasks)
        else:
            return render_template("tasks.html")

    def post(self):
        body = request.json
        body["user_id"] = current_user.id

        new_task = Task.create_from_json(json_body=body)
        task_database_api.add(new_task)
        for element in body:
            if element.startswith("notification") and body.get(element):
                new_notification = Notification(
                    task_id=new_task.id, date_and_time=body.get(element)
                )
                notification_database_api.add(new_notification)
                print(body)
        return redirect(url_for("tasks"))

    def put(self, id):
        body = request.json
        known_task = task_database_api.get_by_id(id)
        if known_task:
            body["user_id"] = current_user.id
            new_task = Task.create_from_json(json_body=body)
            task_database_api.update_whole_by_id(id=id, item_to_put=new_task)
            return "Success", 200
        return "Not found", 404

    def delete(self, id):
        known_task = task_database_api.get_by_id(id)
        if known_task:
            task_database_api.delete_by_id(id)
            return "Success", 200
        return "Not found", 404


class Notifications(MethodView):
    decorators = [login_required]

    def post(self, id):
        body = request.json
        body["date_and_time"] = body["newInputValue"]
        body["task_id"] = id
        new_notification = Notification.create_from_json(body)
        notification_database_api.add(new_notification)
        return "Success", 200

    def put(self, id):
        body = request.json
        known_notification = notification_database_api.get_by_id(id)
        if known_notification:
            new_notification = Notification(
                date_and_time=body["newInputValue"], task_id=known_notification.task_id
            )
            notification_database_api.update_whole_by_id(id, new_notification)
            return "Success", 200
        return "Not found", 404

    def delete(self, id):
        known_notification = notification_database_api.get_by_id(id)
        if known_notification:
            notification_database_api.delete_by_id(id)
            return "Success", 200
        return "Not found", 404

# Task Scheduler

## Table of contents

* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Roadmap](#roadmap)
* [Project status](#project-status)

## General info
<details>
<summary>Click here to see general information about <b>Task Scheduler</b>!</summary>
<b>This is a portfolio project best described as my version of Google Calendar</b>.
It is a web app allowing users to schedule their tasks and set up notifications which they'll
receive by email. User can create an account or log in with google. There is a possibility of
changing all user's information when wanted. User can freely view, add, delete and
modify their tasks and notifications. Notifications can be individually or collectively
hidden.

</details>

## Technologies
<details>
<summary>Click here to see technologies used in the project</summary>
<ul>
<li>Flask</li>
<li>PostgreSQL</li>
<li>Docker</li>
<li>Pipenv</li>
<li>Bulma</li>
</ul>

</details>

## Setup
<details>
<summary>A guide for setting up the app yourself</summary>

Development setup:

<li>Install Python, Postgresql and Pipenv</li>
<li>Download the files from this repository</li>
<li>Run "pipenv install" command</li>
<li>Create .env file based on .env.example with real values. For all google values check OAuth 2 setup</li>
<li>You can now run the app with "flask run" command</li>

Production setup:

<li>Setup will be done using Docker compose, currently all docker files and their contents are placeholders.</li>


</details>

## Roadmap
<details>
<summary>Click here to see project roadmap</summary>
<ul>
<li> [x] Create basic project structure</li>
<li> [x] Add On-premise login and registration</li>
<li> [x] Add Google login</li>
<li> [x] Add tasks and notifications CRUD</li>
<li> [x] Allow users to reset their password and edit their profile</li>
<li> [ ] Add notifications sending</li>
<li> [ ] Deploying app using docker</li>
</ul>
</details>



## Project status
In developement
